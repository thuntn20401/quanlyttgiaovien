﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTTGiaoVien
{
    public class NguoiLaoDong
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }

        public NguoiLaoDong()
        {

        }

        public NguoiLaoDong(string hoten, int namsinh, double luongcoban)
        {
            this.HoTen = hoten;
            this.NamSinh = namsinh;
            this.LuongCoBan = luongcoban;
        }

        public virtual void NhapThongTin(string hoten, int namsinh, double luongcoban)
        {
            Console.Write("Ho ten: ");
            HoTen = Console.ReadLine();
            Console.Write("Nam sinh: ");
            NamSinh= Convert.ToInt32(Console.ReadLine());
            Console.Write("Luong co ban: ");
            LuongCoBan = Convert.ToDouble(Console.ReadLine());

        }
        public virtual void XuatThongTin()
        {
            Console.Write("Ho ten la: {0}, Nam sinh: {1}, Luong co ban: {2}",HoTen, NamSinh, LuongCoBan);
        }

        public virtual double TinhLuong()
        {
            return this.LuongCoBan;
        }

        
    }
}
