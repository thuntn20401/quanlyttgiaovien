﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTTGiaoVien
{
    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien()
        {

        }

        public GiaoVien(string hoten, int namsinh, double luongcoban, double hesoluong)
        {
            this.HoTen = hoten;
            this.NamSinh = namsinh;
            this.LuongCoBan = luongcoban;
            this.HeSoLuong = hesoluong;
        }

        public override void NhapThongTin(string hoten, int namsinh, double luongcoban)
        {
            base.NhapThongTin(HoTen, NamSinh, LuongCoBan);
            Console.Write("Nhap he so luong: ");
            HeSoLuong = Convert.ToDouble(Console.ReadLine());
        }

        public override double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public override void XuatThongTin()
        {
            base.XuatThongTin();
            double Luong = TinhLuong();
            Console.WriteLine(", He so luong: {0}, Luong: {1}", HeSoLuong, Luong);
            Console.ReadLine();
        }

        public List<GiaoVien> giaovien = new List<GiaoVien>();
        public void NhapDanhSachGV()
        {
            do
            {
                //NguoiLaoDong nld = new NguoiLaoDong();
                GiaoVien gv = new GiaoVien();
                gv.NhapThongTin(gv.HoTen, gv.NamSinh, gv.LuongCoBan);
                giaovien.Add(gv);
                Console.Write("Ban muon them du lieu khong? (y/n): ");
                string st = Console.ReadLine();
                if (st == "n")
                    break;
            } while (true);
        }
        








    }
}